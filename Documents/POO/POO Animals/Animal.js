

function Animal(n){

    //-- paramètres

    this.nom = n;
    this.age = 0;
    this.naissance = new Date();

    


    //-- Méthodes

    this.toHTML = function(){
        var node = document.createElement("DIV");
        var textnode = document.createTextNode(n);
        node.appendChild(textnode);
        document.getElementById("bestiaire").appendChild(node);
    }
    
    //-- Autres actions réalisée à la construction (constructeur)

    this.toHTML();
}